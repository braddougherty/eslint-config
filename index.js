'use strict';

module.exports = {
	rules: {
		'arrow-parens': ['error', 'always'],
		'object-curly-spacing': ['error', 'always']
	}
};
